import './App.css';
import React, { useState } from 'react';
import styled from "styled-components";
// 
import ChillColorPicker from './ChillColorPicker';

const randomColor = () => {
  const randomColor = "#000000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);});
  console.log(randomColor);
  return randomColor;
}
const randomDegree = () => {
  return Math.floor(Math.random()*360);
}

// TODO: do keyframes programatically one day...
const chillBG = `@keyframes {
  0% {
    background-position: 0% 0%;
  }
  25% {
    background-position: 100% 0%;
  }
  50% {
    background-position: 100% 100%;
  }
  75% {
    background-position: 0% 100%;
  }
  100% {
    background-position: 0% 0%;
  }
}`;

const SuperNiceBG = (props) => {
  return styled.header`
    background-color: ${props.bgColor};
    background: ${props.bgString};
    background-blend-mode: ${props.bgBlend};
    animation: chillBG ${props.animationDuration}s linear infinite;
    background-size: ${props.bgSize + '%'};
    background-position: 0% 0%; // this must match what is in the chillBG animation in App.css
  `;
}

function App() {
  // Helper Functions
  const setQueryStringWithNoReload = qsValue => {
    const newURL = window.location.protocol + '//' +
      window.location.host +
      window.location.pathname +
      qsValue;
    console.log(newURL);
    window.history.pushState({ path: newURL }, "", newURL);
  }
  const urlSearch = window.location.search
  let urlParams = new URLSearchParams(urlSearch);
  // Grab the settings from the URL
  const urlWords = urlParams.get('words');
  const urlBgColor = urlParams.get('bgColor');
  const urlCurGradients = urlParams.get('curGradients');
  // TODO: Maybe delete bg size and animationDuration for now?
  const urlBgSize = urlParams.get('bgSize');
  const urlAnimationDuration = urlParams.get('animationDuration');

  const [wordlist, setWordlist] = useState(
      urlWords ? urlWords.split(',') :
      [
        'Chill',
        '',
        // '🏖','🏝','🌋','⛰','🏔','🏕','🏜',
        // '⛵️','🛶','🚲','🎣','☀️',
        // '💎','🔮','🧬','🧘',
        // 'Relax',
      ]
    );
  const [count, setCount] = useState(0);
  const [bgColor, setBgColor] = useState(urlBgColor ? urlBgColor : '#000000');
  const changeBgColor = color => setBgColor(color);
  // const bgSize = 100;
  // const animationDuration = 0;
  const [bgSize, setBgSize] = useState(urlBgSize ? urlBgSize : 100);
  const changeBgSize = e => setBgSize(e.target.value);
  const [animationDuration, setAnimationDuration] = useState(urlAnimationDuration ? urlAnimationDuration : 0);
  const changeAnimationDuration = e => setAnimationDuration(e.target.value);
  const [curGradients, setCurGradients] = useState(urlCurGradients ? JSON.parse(urlCurGradients) : [
    {deg: 0, from: '#00AE41', to: '#FFAE41'},
    {deg: 90, from: '#00AE41', to: '#0000FF'},
    // {deg: 190, from: '#ffAE41', to: '#FFAE41'},
  ]);
  const handleChange = e => {
    const newRes = e.target.value.split('\n');
    setWordlist(newRes);
  }
  
  const handleDynamicColorChange = (num, name) => color => {
    console.log('color', color, num);
    setCurGradients(curGradients.map((x, count) => {
      if (count === num) {
        let res = {...x};
        res[name] = color;
        return res;
      }
      return x
    }));
  }
  const handleDynamicChange = e => {

    
    const name_parts = e.target.name.split('_');
    const new_gradients = curGradients;
    console.log('e', e, name_parts)
    let new_val = e.target.value;
    console.log('handleDynamicChange',new_val);
    if (name_parts[1] === 'deg') new_val = parseInt(new_val)
    new_gradients[name_parts[0]][name_parts[1]] = new_val;
    setCurGradients(curGradients.map((x, count) => {
      if (count === parseInt(name_parts[0])) {
        let res = {...x};
        res[name_parts[1]] = new_val
        return res;
      }
      return x
    }));
  }
  const deleteRow = (row) => () => {
    // console.log('row', row)
    // const new_gradients = curGradients;
    // const shit = new_gradients.slice(row, 2)
    // console.log(shit)
    // console.log(new_gradients)
    setCurGradients(curGradients.filter((x, count) => count !== row))
  }
  const addRow = () => {

    // setCurGradients([...curGradients, {deg: 90, from: '#00000', to: '#FFFFF'}])
    setCurGradients([...curGradients, {
      deg: randomDegree(),
      from: randomColor(),
      to: randomColor()
    }]);
  }
  const selectText = () => {
    const input = document.getElementById('copy-text');
    input.focus();
    input.setSelectionRange(0, 1000);
  }
  const generate = () => {
    if (count >= wordlist.length) {
      setCount(0);  
    } else {
      setCount(count + 1); 
    }
  }
  
  const save = () => {
    let paramsObj = {
      bgColor: bgColor,
      bgSize: bgSize,
      animationDuration: animationDuration,
      curGradients: JSON.stringify(curGradients),
      words: wordlist,
    }
    console.log('paramsObj', paramsObj);
    const searchParams = new URLSearchParams(paramsObj);
    console.log('searchParams', searchParams.toString());
    setQueryStringWithNoReload(
      `?${searchParams.toString()}`
      //`?bgColor='${bgColor}'`
      //&bgSize=${bgSize}&animationDuration=${animationDuration}&color1="${color1}"&color1deg=${color1deg}&color2=$"{color2}"&color2deg=${color2deg}&words=${wordlist}`
    )
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }
  const inputStyles = {
    margin: '1em',
    width: '90%',
    maxWidth: '800px',
  };
  // let bgString = '';
  // for (let i = 0; i < curGradients.length; i++) {
  //   bgString += `linear-gradient(${parseInt(curGradients[i].deg)}deg, ${curGradients[i].from}, ${curGradients[i].to})`
  //   if (i !== curGradients.length-1) {
  //     bgString += ','
  //   }
  // }
  // bgString = `linear-gradient(${color1deg}deg, ${color1}, ${bgColor}), linear-gradient(${color2deg}deg, ${color2}, ${bgColor})`;
  const bgString = `${curGradients.map((g) => `linear-gradient(${g.deg}deg, ${g.from}, ${g.to})`)}`;
  const bgBlend = 'screen, difference, lighten'; // TODO: Make these a setting as well
  // TODO: We should randomize colors when the page loads?
  // useEffect(() => randomizeColors(), []);


  const ListOfSettings = () => {
    return curGradients?.map((g, count) => (
      <div key={`settings_${count}`} className='settingsRow'>
        {/*
        <p>{count} - {g.deg} - {g.from} - {g.to}</p>
        <pre key={`json_${count}`}>{JSON.stringify(g)}</pre>
        */}
        <input key={`Gradient_${count}_Text`} type='text' className='very-little-text' label='Gradient Angle' name={`${count}_deg`} value={g.deg} onChange={handleDynamicChange} />
        <input key={`Gradient_${count}`} type='range' min='0' max='360' label='Gradient' name={`${count}_deg`} value={parseInt(g.deg)} onInput={handleDynamicChange} className='degreeRange' />
        
        {/* <input key={`From_${count}_Text`} type='text' className='little-text' label='From Color' name={`${count}_from`} value={g.from} onChange={handleDynamicChange} /> */}
        <ChillColorPicker color={g.from} onChangeComplete={handleDynamicColorChange(count, 'from')} />
        {/* <SketchPicker key={`From_${count}`} name={`${count}_from`} color={g.from} onChangeComplete={handleDynamicColorChange(count, 'from')} /> */}
        {/* <input key={`From_${count}`} type='color' label='From' name={`${count}_from`} value={g.from} onChange={handleDynamicChange} /> */}
        {/* <input key={`To_${count}_Text`} type='text' className='little-text' label='To Color' name={`${count}_to`} value={g.to} onChange={handleDynamicChange} /> */}
        {/* <input key={`To_${count}`} type='color' label='To' name={`${count}_to`} value={g.to} onChange={handleDynamicChange} /> */}
        <ChillColorPicker color={g.to} onChangeComplete={handleDynamicColorChange(count, 'to')} />
        {/* <SketchPicker key={`To_${count}`} name={`${count}_to`} color={g.to} onChangeComplete={handleDynamicColorChange(count, 'to')} /> */}
        <input key={`Delete_${count}`} type='button' value='⌫' onClick={deleteRow(count)} className='deleteRowButton' />
        {/* TODO: Add a lock button and a randomize button */}
      </div>
    ));
  }
  
  // Very cool styled-component thing here!
  const NiceHeader = SuperNiceBG({
    bgColor: bgColor,
    bgString: bgString,
    bgBlend: bgBlend,
    animationDuration: animationDuration,
    bgSize: bgSize,
  })
  // const NiceHeader = styled.header`
  //   background-color: ${bgColor};
  //   background: ${bgString};
  //   background-blend-mode: ${bgBlend};
  //   animation: ${chillBG} ${animationDuration}s linear infinite;
  //   background-size: ${bgSize + '%'};
  //   background-position: 0% 0%; // this must match what is in the chillBG animation in App.css
  // `;
  // console.log('chillBG', chillBG);
  // TODO: 9/24
  // - Regular Website Header
  // - Fullscreen button
  // - Controls go into modals?
  // - Add back in the randomize button
  // - Animation Studio
  // - Make the centered text thing more clear
  // - Site style should respect user theme
  // - Wow using this keyboard on that screen is huge.
  return (
    <div className="App">
      <NiceHeader
        className="App-header"
        onClick={generate}
      >
        <h1 className='bigWord'>{wordlist[count]}</h1>
      </NiceHeader>
      <div className='settings'>
        <div className='bgColorLabel'>
          Linear Gradients:
        </div>
        <ListOfSettings />
        <div className='settingsRow'>
          <input type='button' value='Add Linear Gradient' onClick={addRow} className='addRowButton' />  
        </div>
        <div className='bgColorLabel'>
          Background Color:
        </div>
        <div className='settingsRow'>
          <ChillColorPicker color={bgColor} onChangeComplete={changeBgColor} />
          {/* <input type='text' className='little-text' label='Background Color' name={`colorBg`} value={bgColor} onChange={changeBgColor} /> */}
          {/* <input type='color' label='Background Color' name={`colorBg`} value={bgColor} onChange={changeBgColor} /> */}
        </div>
        <div>
        <input type='button' value='Save settings into URL' onClick={save} className='saveButton' />
        </div>
        <textarea
          className='code'
          style={inputStyles}
          id='copy-text'
          rows={Object.keys(curGradients).length+2}
          onClick={selectText}
          value={`.myGradientBackground {\n  background: ${bgString.replaceAll('),','),\n    ')}\n}`} readOnly
        />
        {/*
        <input type='text' className='little-text' label='Color 1' name={`color1`} value={color1} onChange={changeColor1} />
        <input type='color' label='Color 1' name={`color1`} value={color1} onChange={changeColor1} />
        <br/>

        <input type='text' className='little-text' label='Angle' name={`color1deg`} value={color1deg} onChange={changeColor1Deg} />
        <input type='range' min='0' max='360' label='Angle' name='color1angle' onChange={changeColor1Deg} value={color1deg} />
        <br/>

        <input type='text' className='little-text' label='Color 2' name={`color2`} value={color2} onChange={changeColor2} />
        <input type='color' label='Color 2' name={`color2`} value={color2} onChange={changeColor2} />
        <br/>

        <input type='text' className='little-text' label='Angle' name='color2angle' onChange={changeColor2Deg} value={color2deg} />
        <input type='range' min='0' max='360' label='Angle' name='color2angle' onChange={changeColor2Deg} value={color2deg} />
        <br/>
        */}
        {/* <input type='button' value='🎲 Randomize 🎲' onClick={randomizeColors}/> */}
        <br/>

        <br/>

        {/* TODO: Animations one day...
        */}
        <p>BG Zoom: {bgSize}</p>
        <input type='range' min='100' max='600' style={inputStyles} label='BG Zoom' name='bgZoom' onChange={changeBgSize} value={bgSize} />
        
        <p>Animation Duration: {animationDuration}s</p>
        <input type='range' min='0' max='60' style={inputStyles} label='Animation Duration' name='animationDuration' onChange={changeAnimationDuration} value={animationDuration} />
        
        <p>{wordlist.length} Lines of Text (Click colorful background to cycle through)</p>
        <textarea style={inputStyles} label='words' name='words' rows={5} onChange={handleChange} value={wordlist.join('\n')}/>
        <br />
        <br />
    
      </div>
      <p>
        <a href='https://gitlab.com/johnpmccabe/chill'>Source Code</a> | Thanks for stopping by!
      </p>
    </div>
  );
}

export default App;
