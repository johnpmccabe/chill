import './App.css';
import React, { useState } from 'react';
import { PhotoshopPicker } from 'react-color';


const ChillColorPicker = props => {
  const [open, setOpen] = useState(false);
  const [color, setColor] = useState(props.color);
  const handleClick = () => {
    if (open) setOpen(false);
    setOpen(true);
  }
  const localAccept = (whuuuuhhh) => {
    props.onChangeComplete(color);
    setOpen(false);
  }
  // const handleClose = setOpen(false);
  const handleChange = (color) => setColor(color.hex);
  const popover = {
    position: 'absolute',
    zIndex: '2',
  }
  const cover = {
    position: 'fixed',
    top: '0px',
    right: '0px',
    bottom: '0px',
    left: '0px',
  }
  return (
    <div>
      <div
        onClick={handleClick}
        style={{
          margin: '2px',
          backgroundColor: color,
          padding: '10px',
          borderRadius: '4px',
          cursor: 'pointer',
        }}
      >
        <div style={{
          fontFamily: 'monospace',
          color: color,
          filter: 'invert(1)', // TODO: Look into blend modes
        }}>
          {color}
        </div>
      </div>
      { open ?
        <div style={open ? popover : cover}>
        <PhotoshopPicker
          color={color}
          // onChange={handleChange}
          onChangeComplete={handleChange}
          onAccept={localAccept}
          onCancel={() => setOpen(false)}
        />
        </div> :
        null
      }
    </div>
    );
}

export default ChillColorPicker;
