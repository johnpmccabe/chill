# Chill

A tool for generating chill gradients and background animations.

## Demo

[![Screenshot of the Chill app](./screenshot.png)*Click to try Chill now*](https://chill-sigma.vercel.app/)

![Screenshot of the Chill app settings](./screenshot2.png)*Choose your own settings and copy the css to use in your own project*


## Running Locally

1. Clone the project to your local machine

1. Use yarn with node 14 or higher.

		nvm install 14 && nvm use 14

1. Then install local dependencies using yarn

		yarn

1. Then launch

		yarn start


## Credits

This project was forked from [namegen](https://gitlab.com/johnpmccabe/namegen) which was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


Shout out to https://favicon.io/favicon-generator/ for help with the favicon!
